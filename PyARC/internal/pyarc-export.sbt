<?xml version="1.0"?>
<SMTK_AttributeSystem Version="2">
  <Definitions>
    <AttDef Type="ExportSpec" Label="Settings" BaseType="" Version="0">
      <ItemDefinitions>
        <String Name="Analysis" Label="Analysis" Version="">
          <DiscreteInfo>
            <Value Enum="DIF3D">dif3d</Value>
            <Value Enum="MCC3">mcc3</Value>
            <Value Enum="MCC3 + DIF3D">mcc3.dif3d</Value>
          </DiscreteInfo>
        </String>
        <File Name="OutputFile" Label="Output File" Version="0" NumberOfRequiredValues="1"
          FileFilters="PyARC files (*.son);;All files (*.*)">
          <BriefDescription>The pyarc file to write</BriefDescription>
        </File>
        <File Name="PythonScript" Label="Python Script" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1"
          ShouldExist="true" FileFilters="Python files (*.py);;All files (*.*)">
          <DefaultValue>NEAMS.py</DefaultValue>
        </File>
      </ItemDefinitions>
    </AttDef>
  </Definitions>

  <Views>
    <View Type="Instanced" Title="Export Settings" TopLevel="true" FilterByCategory="false">
      <InstancedAttributes>
        <Att Name="Options" Type="ExportSpec" />
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeSystem>
