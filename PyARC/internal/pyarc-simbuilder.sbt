<?xml version="1.0"?>
<SMTK_AttributeSystem Version="2">
  <Includes>
    <File>templates/dif3d.sbt</File>
    <File>templates/mcc3.sbt</File>
  </Includes>

  <Views>
    <View Type="Group" Name="PyARC" Label="PyARC" TopLevel="true"
      TabPosition="North" FilterByCategory="false" FilterByAdvanceLevel="false">
      <Views>
        <View Title="DIF3D" />
        <View Title="MCC3" />
      </Views>
    </View>

    <View Type="Instanced" Title="DIF3D" Label="DIF3D">
      <InstancedAttributes>
        <Att Name="dif3d-instance" Type="dif3d" />
      </InstancedAttributes>
    </View>

    <View Type="Instanced" Title="MCC3" Label="MCC3">
      <InstancedAttributes>
        <Att Name="mcc3-instance" Type="mcc3" />
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeSystem>
