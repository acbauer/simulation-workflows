<!-- attributes for turbulenceProperties -->
<SMTK_AttributeSystem Version="2">
  <Categories>
    <Cat>Fluid Flow</Cat>
  </Categories>
  <!-- Attribute Definitions-->
  <Definitions>
    <!--   <AttDef Type="turbulenceModelCategory" Label="Turbulence Model" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <String Name="turbulenceModelCategory" Label="Turbulence Model Category">
          <DiscreteInfo DefaultIndex="0">
            <Value>Laminar"</Value>
            <Value>RAS</Value>
            <Value>LES</Value>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef> -->
    <AttDef Type="printCoeffs">
      <ItemDefinitions>
        <Void Name="printCoeffs" Label="Print Coefficients" Optional="true" IsEnabledByDefault="true">
          <BriefDescription>Option to print turbulence model coefficients at simulation start up</BriefDescription>
        </Void>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="rasModels">
      <ItemDefinitions>
        <String Name="rasModel" Label="RAS Model">
          <DiscreteInfo>
            <Value>LRR</Value>
            <Value>LamBremhorstKE</Value>
            <Value>LaunderSharmaKE</Value>
            <Value>LienCubicKE</Value>
            <Value>LienLeschziner</Value>
            <Value>RNGkEpsilon</Value>
            <Value>SSG</Value>
            <Value>ShihQuadraticKE</Value>
            <Value>SpalartAllmaras</Value> <!-- used in the airFoil2D tutorial problem -->
            <Value>kEpsilon</Value>
            <Value>kOmega</Value>
            <Value>kOmegaSST</Value> <!-- used in the motorBike tutorial problem -->
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="lesModels">
      <ItemDefinitions>
        <String Name="lesModel" Label="LES Model">
          <DiscreteInfo>
            <Value>DeardorffDiffStress</Value>
            <Value>Smagorinsky</Value>
            <Value>SpalartAllmarasDDES</Value>
            <Value>SpalartAllmarasDES</Value>
            <Value>SpalartAllmarasIDDES</Value>
            <Value>WALE</Value>
            <Value>dynamicKEqn</Value>
            <Value>dynamicLagrangian</Value>
            <Value>kEqn</Value>
            <Value>kOmegaSSTDES</Value>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="SpalartAllmaras">
      <ItemDefinitions>
        <Double Name="sigmaNut" Label="sigmaNut">
          <DefaultValue>0.66666</DefaultValue>
        </Double>
        <Double Name="kappa" Label="kappa">
          <DefaultValue>.41</DefaultValue>
        </Double>

        <Double Name="Cb1" Label="Cb1">
          <DefaultValue>.1355</DefaultValue>
        </Double>
        <Double Name="Cb2" Label="Cb2">
          <DefaultValue>.622</DefaultValue>
        </Double>
        <Double Name="Cw2" Label="Cw2">
          <DefaultValue>.3</DefaultValue>
        </Double>
        <Double Name="Cw3" Label="Cw3">
          <DefaultValue>2</DefaultValue>
        </Double>
        <Double Name="Cv1" Label="Cv1">
          <DefaultValue>7.1</DefaultValue>
        </Double>
        <Double Name="Cs" Label="Cs">
          <DefaultValue>.3</DefaultValue>
        </Double>
        <!-- initial conditions -->
        <Double Name="initialNut" Label="Initial Nut">
          <DefaultValue>0.14</DefaultValue>
        </Double>
        <Double Name="initialNuTilda" Label="Initial NuTilda">
          <DefaultValue>.14</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="kOmegaSST">
      <ItemDefinitions>
        <Double Name="alphaK1" Label="alphaK1">
          <DefaultValue>0.85</DefaultValue>
        </Double>
        <Double Name="alphaK2" Label="alphaK2">
          <DefaultValue>1</DefaultValue>
        </Double>
        <Double Name="alphaOmega1" Label="alphaOmega1" Optional="true" IsEnabledByDefault="true">
          <DefaultValue>.5</DefaultValue>
        </Double>
        <Double Name="alphaOmega2" Label="alphaOmega2" Optional="true" IsEnabledByDefault="true">
          <DefaultValue>.856</DefaultValue>
        </Double>
        <Double Name="gamma1" Label="gamma1" Optional="true" IsEnabledByDefault="true">
          <DefaultValue>0.555556</DefaultValue>
        </Double>
        <Double Name="gamma2" Label="gamma2" Optional="true" IsEnabledByDefault="true">
          <DefaultValue>0.44</DefaultValue>
        </Double>
        <Double Name="beta1" Label="beta1" Optional="true" IsEnabledByDefault="true">
          <DefaultValue>0.075</DefaultValue>
        </Double>
        <Double Name="beta2" Label="beta2" Optional="true" IsEnabledByDefault="true">
          <DefaultValue>0.0828</DefaultValue>
        </Double>
        <Double Name="betaStar" Label="betaStar" Optional="true" IsEnabledByDefault="true">
          <DefaultValue>0.09</DefaultValue>
        </Double>
        <Double Name="a1" Label="a1" Optional="true" IsEnabledByDefault="true">
          <DefaultValue>.31</DefaultValue>
        </Double>
        <Double Name="b1" Label="b1" Optional="true" IsEnabledByDefault="true">
          <DefaultValue>1</DefaultValue>
        </Double>
        <Double Name="c1" Label="c1" Optional="true" IsEnabledByDefault="true">
          <DefaultValue>10</DefaultValue>
        </Double>
        <Void Name="F3" Label="F3" Optional="true" IsEnabledByDefault="true" />
        <!-- initial conditions -->
        <Double Name="initialTurbulentKE" Label="Initial KE">
          <DefaultValue>0.24</DefaultValue>
        </Double>
        <Double Name="initialTurbulentOmega" Label="Initial Omega">
          <DefaultValue>1.78</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <!-- wall functions: not really a boundary condition but applied like a boundary condition -->
    <AttDef Type="nut-wall-function" Label="nut" BaseType="" Version="0" Unique="true">
      <AssociationsDef Name="ModelAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="wallFunctionValue" Label="Value" />
        <String Name="wallFunctionType" Label="Type" >
          <DiscreteInfo>
            <Value Enum="Calculated">calculated</Value>
            <Value Enum="NUT U">nutuWallFunction</Value>
            <Value Enum="NUT K">nutkWallFunction</Value>
            <Value Enum="NUT U Rough">nutuRoughWallFunction</Value>
            <Value Enum="NUT K Rough">nutkRoughWallFunction</Value>
            <Value Enum="NUT U">nutUWallFunction</Value>
            <Value Enum="NUT K Atm">KnutkAtmRoughWallFunction</Value>
            <Value Enum="NUT U Tabulated">nutUTabulatedWallFunction</Value>
            <Value Enum="NUT">nutWallFunction</Value>
            <Value Enum="NUT Low Re">nutLowReWallFunction</Value>
            <Value Enum="NUT U Spalding">nutUSpaldingWallFunction</Value>
            <Value Enum="NUT Freestream">freestream</Value>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <!-- Boundary Conditions for SpalartAllmaras -->
    <AttDef Type="nuTilda" Label="Nu Tilda" BaseType="" Version="0" Unique="true">
      <AssociationsDef Name="ModelAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="Nu Tilda" Label="Value" />
      </ItemDefinitions>
    </AttDef>
    <!-- Boundary Conditions for kOmegaSST -->
    <AttDef Type="ke-boundary-condition" Label="k" BaseType="" Version="0" Unique="true">
      <AssociationsDef Name="ModelAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="KE" Label="Value" />
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="omega-boundary-condition" Label="omega" BaseType="" Version="0" Unique="true">
      <AssociationsDef Name="ModelAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="Omega" Label="Value" />
      </ItemDefinitions>
    </AttDef>


    <AttDef Type="TurbulenceModelSelector">
      <ItemDefinitions>
        <String Name="Selector" Label="Turbulence Model Class">
          <DiscreteInfo DefaultIndex="0">
            <Value>Laminar</Value>
            <Value>RAS</Value>
            <Value>LES</Value>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
  </Definitions>

  <!--********** Workflow Views ***********-->
  <Views>
    <View Type="Selector" Title="Turbulence" SelectorName="selectorAtt2" SelectorType="TurbulenceModelSelector">
      <DefaultColor>1., 1., 0.5, 1.</DefaultColor>
      <InvalidColor>1, 0.5, 0.5, 1</InvalidColor>
      <Views>
        <View Title="RAS" Enum="RAS"/>
        <View Title="LES" Enum="LES"/>
      </Views>
    </View>

    <View Type="Group" Title="RAS" Style="tiled">
      <Views>
        <View Title="Print Coeffs"/>
        <View Title="RAS Models"/>
      </Views>
    </View>

    <View Type="Group" Title="LES" Style="tiled">
      <Views>
        <View Title="Print Coeffs"/>
        <View Title="LES Models"/>
      </Views>
    </View>

    <View Type="Instanced" Title="Print Coeffs">
      <InstancedAttributes>
        <Att Name="printCoeffs" Type="printCoeffs" />
      </InstancedAttributes>
    </View>

    <View Type="Selector" Title="RAS Models" SelectorName="selectorAtt299" SelectorType="rasModels">
      <Views>
        <View Enum="LRR"/>
        <View Enum="LamBremhorstKE"/>
        <View Enum="LaunderSharmaKE"/>
        <View Enum="LienCubicKE"/>
        <View Enum="LienLeschziner"/>
        <View Enum="RNGkEpsilon"/>
        <View Enum="SSG"/>
        <View Enum="ShihQuadraticKE"/>
        <View Title="SpalartAllmarasView" Enum="SpalartAllmaras"/>
        <View Enum="kEpsilon"/>
        <View Enum="kOmega"/>
        <View Title="kOmegaSSTView" Enum="kOmegaSST"/>
      </Views>
    </View>

    <View Type="Selector" Title="LES Models" SelectorName="selectorAtt300" SelectorType="lesModels">
      <Views>
        <View Enum="DeardorffDiffStress"/>
        <View Enum="Smagorinsky"/>
        <View Enum="SpalartAllmarasDDES"/>
        <View Enum="SpalartAllmarasDES"/>
        <View Enum="SpalartAllmarasIDDES"/>
        <View Enum="WALE"/>
        <View Enum="dynamicKEqn"/>
        <View Enum="dynamicLagrangian"/>
        <View Enum="kEqn"/>
        <View Enum="kOmegaSSTDES"/>
      </Views>
    </View>

    <View Type="Group" Title="SpalartAllmarasView" Style="tiled">
      <Views>
        <View Title="Spalart Allmaras Parameters"/>
        <View Title="Spalart Allmaras Boundary Conditions"/>
      </Views>
    </View>

    <View Type="Instanced" Title="Spalart Allmaras Parameters">
      <InstancedAttributes>
        <Att Name="Spalart Allmaras" Type="SpalartAllmaras" />
      </InstancedAttributes>
    </View>
    <View Type="Attribute" Title="Spalart Allmaras Boundary Conditions">
      <AttributeTypes>
        <Att Type="nuTilda"/>
        <Att Type="nut-wall-function"/>
      </AttributeTypes>
    </View>



    <View Type="Group" Title="kOmegaSSTView" Style="tiled">
      <Views>
        <View Title="kOmegaSST Parameters"/>
        <View Title="kOmegaSST Boundary Conditions"/>
      </Views>
    </View>

    <View Type="Instanced" Title="kOmegaSST Parameters">
      <InstancedAttributes>
        <Att Name="kOmegaSST" Type="kOmegaSST" />
      </InstancedAttributes>
    </View>
    <View Type="Attribute" Title="kOmegaSST Boundary Conditions">
      <AttributeTypes>
        <Att Type="ke-boundary-condition"/>
        <Att Type="nut-wall-function"/>
        <Att Type="omega-boundary-condition"/>
      </AttributeTypes>
    </View>

  </Views>


</SMTK_AttributeSystem>
