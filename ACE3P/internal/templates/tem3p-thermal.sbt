<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeSystem Version="2">
  <Categories>
    <Cat>TEM3P-Linear-Thermal</Cat>
    <Cat>TEM3P-Nonlinear-Thermal</Cat>
    <Cat>TEM3P-ThermoElastic</Cat>
  </Categories>
  <Definitions>
    <!-- Definitions specific to TEM3P-Thermal-->
    <AttDef Type="HeatSource" BaseType="" Abstract="true" Unique="false" Version="0">
      <AssociationsDef Name="HeatSourceAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>volume</MembershipMask>
      </AssociationsDef>
    </AttDef>

    <AttDef Type="LossyDielectricHeat" Label="Lossy Dielectric Heat Source" BaseType="HeatSource" Unique="true" Version="0">
      <ItemDefinitions>
        <!-- Specify ConditionType as discrete, even though only 1 value (RFHeating) currently used-->
        <String Name="ConditionType" Label="Type" Version="0">
          <Categories>
            <Cat>TEM3P-Linear-Thermal</Cat> 
            <Cat>TEM3P-Nonlinear-Thermal</Cat> 
            <Cat>TEM3P-ThermoElastic</Cat>
          </Categories>
          <ChildrenDefinitions>
            <Double Name="DielectricConstantE" Label="Dielectric Constant" Version="0">
              <BriefDescription>The dielectric constant</BriefDescription>
              <Categories>
                <Cat>TEM3P-Linear-Thermal</Cat> 
                <Cat>TEM3P-Nonlinear-Thermal</Cat> 
                <Cat>TEM3P-ThermoElastic</Cat>
              </Categories>
              <DefaultValue>1.0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0.0</Min>
              </RangeInfo>
            </Double>
            <Double Name="LossTangentE" Label="Loss Tangent" Version="0">
              <BriefDescription>The dielectric loss tangent</BriefDescription>
              <Categories>
                <Cat>TEM3P-Linear-Thermal</Cat> 
                <Cat>TEM3P-Nonlinear-Thermal</Cat> 
                <Cat>TEM3P-ThermoElastic</Cat>
              </Categories>
              <DefaultValue>0.0</DefaultValue>
            </Double>
            <String Name="NERSCDirectory" Label="NERSC Directory" Version="0">
              <BriefDescription>Full path to existing directory on a NERSC file system, where the omega3p or s3p results are stored</BriefDescription>
              <Categories>
                <Cat>TEM3P-Linear-Thermal</Cat> 
                <Cat>TEM3P-Nonlinear-Thermal</Cat> 
                <Cat>TEM3P-ThermoElastic</Cat>
              </Categories>
            </String>
            <Int Name="WhichMode" Label="Mode Number" Version="0">
              <BriefDescription>The mode number calculated from the omega3p or s3p data imported into tem3p</BriefDescription>
              <Categories>
                <Cat>TEM3P-Linear-Thermal</Cat> 
                <Cat>TEM3P-Nonlinear-Thermal</Cat> 
                <Cat>TEM3P-ThermoElastic</Cat>
              </Categories>
              <DefaultValue>0</DefaultValue>
              <RangeInfo>
                <Min Inclusive="true">0</Min>
              </RangeInfo>
            </Int>
            <!-- Specify Method as discrete, even though only 1 value (Powerinput) currently used-->
            <String Name="Method" Label="Method" Version="0">
              <Categories>
                <Cat>TEM3P-Linear-Thermal</Cat> 
                <Cat>TEM3P-Nonlinear-Thermal</Cat> 
                <Cat>TEM3P-ThermoElastic</Cat>
              </Categories>
              <ChildrenDefinitions>
                <Double Name="TargetPowerinput" Label="Target Power Input" Units="W" Version="0">
                  <BriefDescription>Total power input to the system</BriefDescription>
                  <Categories>
                    <Cat>TEM3P-Linear-Thermal</Cat> 
                    <Cat>TEM3P-Nonlinear-Thermal</Cat> 
                    <Cat>TEM3P-ThermoElastic</Cat>
                  </Categories>
                  <DefaultValue>0</DefaultValue>
                </Double>
              </ChildrenDefinitions>
              <DiscreteInfo DefaultIndex="0">
                <Structure>
                  <Value Enum="Power Input">Powerinput</Value>
                  <Items>
                    <Item>TargetPowerinput</Item>
                  </Items>
                </Structure>
              </DiscreteInfo>
            </String>
          </ChildrenDefinitions>
          <!-- ConditionType-->
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="RF Heating">RFHeating</Value>
              <Items>
                <Item>DielectricConstantE</Item>
                <Item>LossTangentE</Item>
                <Item>NERSCDirectory</Item>
                <Item>WhichMode</Item>
                <Item>Method</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="ExternalVolumeHeating" Label="External Volume Heating" BaseType="HeatSource" Version="0">
      <ItemDefinitions>
        <String Name="ConditionType" AdvanceLevel="99">
          <DefaultValue>ExtVHeating</DefaultValue>
          <Categories>
            <Cat>TEM3P-Linear-Thermal</Cat> 
            <Cat>TEM3P-Nonlinear-Thermal</Cat> 
            <Cat>TEM3P-ThermoElastic</Cat>
          </Categories>
        </String>
        <File Name="ExtVHeatingFile" Label="External Heating Map File" ShouldExist="true" NumberOfRequiredValues="1" Version="0">
          <BriefDescription>File name of the external volume heating data</BriefDescription>
          <Categories>
            <Cat>TEM3P-Linear-Thermal</Cat> 
            <Cat>TEM3P-Nonlinear-Thermal</Cat> 
            <Cat>TEM3P-ThermoElastic</Cat>
          </Categories>
        </File>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="ThermalShell" Label="Thermal" BaseType="" Unique="true" Version="0">
      <AssociationsDef Name="ThermalShellAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Int Name="BasisOrder" Label="Basis Order" Version="0">
          <BriefDescription>Order of the basis functions of shell elements</BriefDescription>
          <Categories>
            <Cat>TEM3P-Linear-Thermal</Cat> 
            <Cat>TEM3P-Nonlinear-Thermal</Cat> 
            <Cat>TEM3P-ThermoElastic</Cat>
          </Categories>
          <DefaultValue>1</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0</Min>
          </RangeInfo>
        </Int>
        <Double Name="Thickness" Label="Thickness" Version="0">
          <BriefDescription>Thickness of the shell</BriefDescription>
          <Categories>
            <Cat>TEM3P-Linear-Thermal</Cat> 
            <Cat>TEM3P-Nonlinear-Thermal</Cat> 
            <Cat>TEM3P-ThermoElastic</Cat>
          </Categories>
          <DefaultValue>0.0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
        <String Name="GeneralThermalConductivity" Label="Material" Version="0">
          <Categories>
            <Cat>TEM3P-Nonlinear-Thermal</Cat> 
            <Cat>TEM3P-ThermoElastic</Cat>
          </Categories>
          <ChildrenDefinitions>
            <Double Name="ConstantThermalConductivity" Label="Thermal Conductivity" Units="W/m*K" Version="0">
              <BriefDescription>Thermal conductivity</BriefDescription>
              <Categories>
                <Cat>TEM3P-Nonlinear-Thermal</Cat> 
                <Cat>TEM3P-ThermoElastic</Cat>
              </Categories>
            </Double>
            <File Name="NonlinearThermalConductivity" Label="Nonlinear Thermal Conductivity (File)" NumberOfRequiredValues="1" ShouldExist="true" Version="0">
              <BriefDescription>File that defines the function for the material nonlinearity</BriefDescription>
              <Categories>
                <Cat>TEM3P-Nonlinear-Thermal</Cat> 
                <Cat>TEM3P-ThermoElastic</Cat>
              </Categories>
            </File>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <!-- The values are filenames to be included in the input deck-->
            <Value Enum="Aluminum 6061">AL6061</Value>
            <Value Enum="Aluminum 6063">AL6063</Value>
            <Value Enum="AluminumOxide">Al203</Value>
            <Value Enum="Copper with RRR10">CURRR10</Value>
            <Value Enum="Copper with RRR30">CURRR30</Value>
            <Value Enum="Copper with RRR50">CURRR50</Value>
            <Value Enum="Copper with RRR100">CURRR100</Value>
            <Value Enum="Nb with RRR300">NbRRR300</Value>
            <Value Enum="NbTi">NbTi</Value>
            <Value Enum="Plastic Peek">Peek</Value>
            <Value Enum="Regular Nb">RGNB</Value>
            <Value Enum="Silicon Bronze">SiliconBronze</Value>
            <Value Enum="Stainless Steel 316">SS316</Value>
            <Structure>
              <Value Enum="Custom - Constant...">CustomConstant</Value>
              <Items>
                <Item>ConstantThermalConductivity</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Custom - Nonlinear...">CustomNonlinear</Value>
              <Items>
                <Item>NonlinearThermalConductivity</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
        <Double Name="ConstantThermalConductivity" Label="Thermal Conductivity" Units="W/m*K" Version="0">
          <BriefDescription>Thermal conductivity</BriefDescription>
          <Categories>
            <Cat>TEM3P-Linear-Thermal</Cat>
          </Categories>
        </Double>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeSystem>
