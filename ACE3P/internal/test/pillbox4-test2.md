# Test Conditions in pillbox4-test1.crf

## Boundary conditions
SurfaceProperty - Type Impedance
* side set 1

Surface Property - Type Port
* side set 2
* Number of modes - 2

## Materials
Material
* element block 1
* Relative Permittivity (Epsilon) 0.975
* Relative Permeability (Mu) 0.6

## Analysis
Tolerant - checked

Post Process
* Write Post Process Files - checked
* Model Files Prefix - mfile
